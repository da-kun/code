package com.demo.vo.acl;

import lombok.Data;

/**
 * @author xiao
 */
@Data
public class AssignVo {

    private Long roleId;

    private Long[] permissionId;
}
