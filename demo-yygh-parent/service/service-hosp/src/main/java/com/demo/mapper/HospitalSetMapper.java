package com.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.demo.model.hosp.HospitalSet;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author xiao
 */
@Mapper
public interface  HospitalSetMapper extends BaseMapper<HospitalSet> {
}
