package com.demo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.demo.model.hosp.HospitalSet;

/**
 * @author xiao
 */
public interface HospitalSetService extends IService<HospitalSet> {
}
